**isItProspect** © Léo Gervet 2021 for Kaffein.agency.<br>
Special thanks to Mickaël Wala

Growth-hacking python 3 utility to check websites' improvements possibilities in bulk. It checks _technical SEO_ (h1 number, meta-tags, images alt attributes, page title), _SSL validation_, _Google Lighthouse scores_, and scraps _emails_ and _phone numbers_.

A recent update adds a specific feature for one the agency's specific prospection need : it checks the number of internal links on the page.

-----

**USAGE**

Needs `Requests` and `BeautifulSoup` modules.<br>
`python3 isItProspect.py urlList`

urlList is the text document from which bulk websites are stored. <br>
**/!\ One website per line, without `http(s)://www`**. There is at the moment no rewrite, so this must be respected.
