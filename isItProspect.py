# isItProspect by Léo Gervet - 2021

import requests, sys, json, datetime, csv, re
from bs4 import BeautifulSoup as bs

i = 0
seoCriterias = 0
isSslValidated = ''
mobilePagespeedScore100 = 0
desktopPagespeedScore100 = 0
isSeetchyProspect = 0
numberOfInternalLinks = 0
contactTels = 0
contactMails = 0
fileSuccess = open('logs.txt', 'a')
csvSuccess = open('output.csv', 'w', newline='')
csvWrite = csv.writer(csvSuccess)

def parseList(filePath):
    list = []
    try:
        with open(filePath) as fileContent:
            adresses = fileContent.readlines()
            for currentUrl in adresses:
                if currentUrl.find('\n') != -1:
                    list.append(currentUrl.split('\n')) 
    except:
        return False
    return list

global urlList
urlList = parseList(sys.argv[1])

def run_seocheck(currentUrl):
  global seoCriterias
  print('Testing '+currentUrl+' ...  '+str(i+1)+'/'+str(len(urlList)))
  fileSuccess.write('Results of '+currentUrl+' ...\n')

  request = requests.get('http://'+currentUrl)
  html = request.text
  soup = bs(html, 'html.parser')

  # 1. Analyse du SEO technique
  print ('     Analyzing SEO :')
  fileSuccess.write('     SEO Analysis :\n')
  # 1.1 nombre de h1
  h1 = soup.find_all('h1')
  if len(h1) > 1 :
    print('          [X] %s <h1> tag(s) found' % len(h1))
    fileSuccess.write('          [X] %s <h1> tag(s) found\n' % len(h1))
    seoCriterias+=1
  else :
    print('          [ ] One <h1> tag found')
    fileSuccess.write('          [ ] One <h1> tag found\n')

  # 1.2 présence de title
  title = soup.find('title')
  if len(title) < 1 :
    print('          [X] No <title> tag found')
    fileSuccess.write('          [X] No <title> tag found\n')
    seoCriterias+=1
  else:
    print('          [ ] Title tag found')
    fileSuccess.write('          [ ] Title tag found\n')

  # 1.3 présence de alt
  allImagesWithoutAltAttr = soup.find_all('img', alt=False)
  if len(allImagesWithoutAltAttr) > 0 :
    print('          [X] <img> tag without alt attribute found')
    fileSuccess.write('          [X] <img> tag without alt attribute found\n')
    seoCriterias+=1
  else :
    print('          [ ] <img> tags have alt attribute')
    fileSuccess.write('          [ ] <img> tags have alt attribute\n')

  # 1.3 présence de alt remplis
  imagesWithAltAttr = soup.find_all('img', alt=True)
  if imagesWithAltAttr != None :
    for img in imagesWithAltAttr :
      if len(img.get('alt')) == 0 :
          print('          [X] Empty <img> alt attribute found')
          fileSuccess.write('          [X] Empty <img> alt attribute found\n')
          seoCriterias+=1
      break

  # 1.4 présence de meta-description
  metaDescription = soup.find('meta', attrs={'name': 'description'})
  if metaDescription is not None :
    for tag, value in metaDescription.attrs.items() :
      if tag == 'content' and len(value) == 0 :
        print('          [X] <meta> description empty')
        fileSuccess.write('          [X] <meta> description empty\n')
        seoCriterias+=1
      elif tag == 'content' and len(value) > 0 :
        print('          [ ] <meta> description filled')
        fileSuccess.write('          [ ] <meta> description filled\n')
    else :
        print('          [X] No <meta> description found')
        fileSuccess.write('          [X] No <meta> description found\n')
        seoCriterias+=1

def run_sslcheck(currentUrl):
    global isSslValidated
    print('     Checking SSL :')
    fileSuccess.write('     SSL Check :\n')
    try:
        sslCheckRequest = requests.get('https://'+currentUrl)
        sslCheckHtml = sslCheckRequest.text
        sslSoup = bs(sslCheckHtml, 'html.parser')
        htmlTag = sslSoup.find('html')
        print('          [ ] SSL Certificate found')
        fileSuccess.write('          [ ] SSL Certificate found\n')
        isSslValidated = 'Yes'
    except requests.exceptions.SSLError :
        print('          [X] SSL Certificate not found')
        fileSuccess.write('          [X] SSL Certificate not found\n')
        isSslValidated = 'No'

def run_pagespeed(currentUrl):
  global mobilePagespeedScore100, desktopPagespeedScore100
  print('     Analyzing page speed ...')
  fileSuccess.write('     Page speed analysis :\n')
  request_url = 'http://'+currentUrl
  request_fields = 'lighthouseResult/categories/*/score'
  apikey = ' AIzaSyCAzepzNQQjDm7_oC4YvD4OMqrc6DIdeR4'
  serviceurl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed'

  mobileParameters = { 'url': request_url,
        'fields': request_fields,
        'key': apikey,
        'strategy': 'MOBILE',
        'category':['PERFORMANCE'],
        'locale': 'fr' }

  mobileRequest = requests.get(serviceurl, params = mobileParameters)
  
  mobileRequestCheckScore = mobileRequest.text
  mobilePagespeedSoup = bs(mobileRequestCheckScore, 'html.parser')
  mobilePagespeedResponse = json.loads(mobilePagespeedSoup.text)

  mobilePagespeedScore = mobilePagespeedResponse['lighthouseResult']['categories']['performance']['score']
  mobilePagespeedScore100 = mobilePagespeedScore*100
  if mobilePagespeedScore100 <= 15 :
    print('          [X] Poor mobile Pagespeed Insight score : '+str(mobilePagespeedScore100))
    fileSuccess.write('          [X] Poor mobile Pagespeed Insight score : '+str(mobilePagespeedScore100)+'\n')
  else :
    print('          [ ] Good mobile Pagespeed Insight score : '+str(mobilePagespeedScore100))
    fileSuccess.write('          [ ] Good mobile Pagespeed Insight score : '+str(mobilePagespeedScore100)+'\n')

  desktopParameters = { 'url': request_url,
        'fields': request_fields,
        'key': apikey,
        'strategy': 'DESKTOP',
        'category':['PERFORMANCE'],
        'locale': 'fr' }

  desktopRequest = requests.get(serviceurl, params = desktopParameters)
  desktopRequestCheckScore = desktopRequest.text
  desktopPagespeedSoup = bs(desktopRequestCheckScore, 'html.parser')
  desktopPagespeedResponse = json.loads(desktopPagespeedSoup.text)

  desktopPagespeedScore = desktopPagespeedResponse['lighthouseResult']['categories']['performance']['score']
  desktopPagespeedScore100 = desktopPagespeedScore*100
  if desktopPagespeedScore100 <= 30 :
    print('          [X] Poor desktop Pagespeed Insight score : '+str(desktopPagespeedScore100))
    fileSuccess.write('          [X] Poor desktop Pagespeed Insight score : '+str(desktopPagespeedScore100)+'\n')
  else :
    print('          [ ] Good desktop Pagespeed Insight score : '+str(desktopPagespeedScore100))
    fileSuccess.write('          [ ] Good desktop Pagespeed Insight score : '+str(desktopPagespeedScore100)+'\n')

def run_seetchychecker(currentUrl) :
  global numberOfInternalLinks
  print('     Checking internal links number')
  seetchyRequest = requests.get('http://'+currentUrl)
  seetchyHtml = seetchyRequest.text
  seetchySoup = bs(seetchyHtml, 'html.parser')
  
  htmlLinks = seetchySoup.find_all('a', href=True)
  for htmlLink in htmlLinks :
    if re.search('(?<![\w\d])%s(?![\w\d])' % currentUrl, str(htmlLink)) != None : # we could improve with '/url' if needed with another RegEx
        numberOfInternalLinks += 1
        pass

  if numberOfInternalLinks > 20 :
    print('          [ ] More than 20 internal links (%s) : not a Seetchy prospect' % numberOfInternalLinks)
    fileSuccess.write('          [ ] More than 20 internal links (%s) : not a Seetchy prospect\n' % numberOfInternalLinks)
  elif numberOfInternalLinks <= 20 :
    print('          [X] 20 or less internal links (%s)  : Seetchy prospect' % numberOfInternalLinks)
    fileSuccess.write('          [X] 20 or less internal links (%s) : Seetchy prospect\n' % numberOfInternalLinks)

def run_telandmail(currentUrl):
  global contactTels, contactMails
  print('     Searching for contact emails and tels')
  telandmailRequest = requests.get('http://'+currentUrl)
  telandmailHtml = telandmailRequest.text
  telandmailSoup =  bs(telandmailHtml, 'html.parser')

  mailLinks = telandmailSoup.select('a[href^=mailto]')
  contactMails = re.findall('href=[\"\']mailto:([a-zA-Z0-9-@._]+)[\"\']', str(mailLinks))
  contactMails = str(contactMails).replace('[', '')
  contactMails = contactMails.replace('\'', '')
  contactMails = contactMails.replace(']', '')

  if len(contactMails) != 0 :
    print('          [X] Contact email(s) found')
    fileSuccess.write('          [X] Contact email(s) found : %s\n' % contactMails)
  else :
    print('          [ ] No contact email found')
    fileSuccess.write('          [ ] No contact email found\n')
  
  telLinks = telandmailSoup.select('a[href^=tel]')
  contactTels = re.findall('href=[\"\']tel:([0-9-+]+)[\"\']', str(telLinks))
  contactTels = str(contactTels).replace('[', '')
  contactTels = contactTels.replace('\'', '')
  contactTels = contactTels.replace(']', '')
  
  if len(contactTels) != 0 :
    print('          [X] Contact tel(s) found')
    fileSuccess.write('          [X] Contact tel(s) found : %s\n' % contactTels)
  else :
    print('          [ ] No contact tel found')
    fileSuccess.write('          [ ] No contact tel found\n')
  
  
def main():
    global i, seoCriterias, isSslValidated, mobilePagespeedScore100, desktopPagespeedScore100, numberOfInternalLinks

    csvWrite.writerow(['URL', 'SEO', 'SSL', 'Desktop performance', 'Mobile performance', 'Website rework prospect', 'Seetchy prospect', 'Phone', 'Email'])
    fileSuccess.write('BEGINNING TEST : '+str(len(urlList))+' URLS TO CHECK\n')

    while len(urlList) > i:
      currentUrl = urlList[i]
      # run_seocheck(currentUrl[0])
      # run_sslcheck(currentUrl[0])
      # run_pagespeed(currentUrl[0])
      # run_seetchychecker(currentUrl[0])
      run_telandmail(currentUrl[0])

      print('')
      fileSuccess.write('\n')
      print('------------------------')
      fileSuccess.write('------------------------\n')
      print('')
      fileSuccess.write('\n')

      if seoCriterias > 2 or isSslValidated == 'No' or desktopPagespeedScore100 <= 30 or mobilePagespeedScore100 <= 15:
        isProspect = 'Yes'
      else :
        isProspect = 'No'
      if isProspect == 'Yes' and numberOfInternalLinks <= 35 :
        isSeetchyProspect = 'Yes'
      else :
        isSeetchyProspect = 'No'
      
      csvWrite.writerow([currentUrl[0], '%s/6 technical error(s) found'%seoCriterias, isSslValidated, int(desktopPagespeedScore100), int(mobilePagespeedScore100), isProspect, isSeetchyProspect, contactTels, contactMails])
      i += 1
    fileSuccess.write(str(i)+' URLS checked \n\n')

    print('Done ! Check the output.csv file. If needed, you can check the more detailed logs.txt file. \nHappy prospection ! :)')
    print('')

if __name__ == '__main__' :
    main()
